#include "Fractions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

using namespace std;

int Fractions::gcd(int x,int y) {
    if (y==0) {
        return x;
    }else {
        return gcd(y,x%y);
    }
}

void Fractions::simplify() {
    int g=gcd(this->nu,this->du);
    this->nu=this->nu/g;
    this->du=this->du/g;
}

Fractions::Fractions() {

}

Fractions::Fractions(string s)
{
    x=s;
    ne(s);
}

void Fractions::set_x(string z)
{
    x=z;
    ne(z);
}
int Fractions:: get_xx() {
    return nu;
}
int Fractions:: get_yy() {
    return du;
}

void Fractions::ne(string s)
{
    string m ;
    for (int i = 0 ; i < s.length(); i++){
        if (s[i] == '/') {
            nu = atoi(m.c_str()) ;
            i++;
            m = "" ;
        }
        m += s[i] ;
    }
    du = atoi(m.c_str()) ;
}

string Fractions::get_x()
{
    return x;
}

Fractions::~Fractions()
{
}


Fractions& Fractions::operator=(const Fractions& rhs)
{
    this->x=rhs.x;
    this->du=rhs.du;
    this->nu=rhs.nu;
    return *this;
}

Fractions& Fractions:: operator-(const Fractions& rhs) {
    if(du>rhs.du) {
        int f=0,h=0;
        for(int i=du; i<2*(du*rhs.du); i++) {
            if(i%du==0 && i%rhs.du==0) {
                f=i;
                break;
            }
        }
        h=((f/du)*nu)-((f/rhs.du)*rhs.nu);
        nu=h;
        du=f;
    }else {
        int f=0,h=0;
        for(int i=rhs.du; i<2*(du*rhs.du); i++) {
            if(i%du==0 && i%rhs.du==0) {
                f=i;
                break;
            }
        }
        h=((f/rhs.du)*rhs.nu)-((f/du)*nu);
        nu=h;
        du=f;
    }
    return *this;
}

Fractions& Fractions:: operator+(const Fractions& rhs) {
    if(du>rhs.du) {
        int f=0,h=0;
        for(int i=du; i<2*(du*rhs.du); i++) {
            if(i%du==0 && i%rhs.du==0) {
                f=i;
                break;
            }
        }
        h=((f/du)*nu)+((f/rhs.du)*rhs.nu);
        nu=h;
        du=f;
    }else {
        int f=0,h=0;
        for(int i=rhs.du; i<2*(du*rhs.du); i++) {
            if(i%du==0 && i%rhs.du==0) {
                f=i;
                break;
            }
        }
        h=((f/rhs.du)*rhs.nu)+((f/du)*nu);
        nu=h;
        du=f;
    }
    return *this;
}

Fractions& Fractions:: operator*(const Fractions& rhs) {
    nu=nu*rhs.nu;
    du=du*rhs.du;
    return *this;
}
Fractions& Fractions:: operator/(const Fractions& rhs) {
    nu=nu*rhs.du;
    du=du*rhs.nu;
    return *this;
}

bool Fractions:: operator<(const Fractions& rhs) {
    if((nu/du)<(rhs.nu/rhs.du)) {
        return true;
    }else {
        return false;
    }
}

bool Fractions:: operator>(const Fractions& rhs) {
    if((nu/du)>(rhs.nu/rhs.du)) {
        return true;
    }else {
        return false;
    }
}

bool Fractions:: operator<=(const Fractions& rhs) {
    if((nu/du)<=(rhs.nu/rhs.du)) {
        return true;
    }else {
        return false;
    }
}

bool Fractions:: operator>=(const Fractions& rhs) {
    if((nu/du)>=(rhs.nu/rhs.du)) {
        return true;
    }else {
        return false;
    }
}

bool Fractions:: operator==(const Fractions& rhs) {
    if((nu/du)==(rhs.nu/rhs.du)) {
        return true;
    }else {
        return false;
    }
}

