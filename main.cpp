#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "Fractions.h"
#include "FractionCalculator.h"
using namespace std;

ostream &operator<<(ostream &os,Fractions &f){
    os<<f.nu<<"/"<<f.du;
}
istream &operator>>(istream &os,Fractions &s) {
    os>>s.x;
    s.ne(s.x);
    return os;

}

ostream &operator<<(ostream &os,FractionCalculator &f){
    os<<f.res.get_xx()<<"/"<<f.res.get_yy();
}

int main()
{
    FractionCalculator f;
    Fractions x;
    x.set_x("1/2");
    Fractions y;
    y.set_x("1/3");
    f.add(x,y);
    cout<<f<<endl;
    if(x==y) {
        cout<<"ah"<<endl;
    }
    else {
        cout<<"la2"<<endl;
    }
    x.simplify();
    cout<<x<<endl;
    return 0;
}
