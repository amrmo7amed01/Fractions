#ifndef FRACTIONS_H
#define FRACTIONS_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

class Fractions
{
    public:
        Fractions();
        Fractions(string s);
        ~Fractions();
        int get_xx();
        int get_yy();
        friend ostream &operator<<(ostream &os,Fractions &f);
        Fractions& operator=(const Fractions& other);
        Fractions& operator+(const Fractions& rhs);
        Fractions& operator-(const Fractions& rhs);
        Fractions& operator*(const Fractions& rhs);
        Fractions& operator/(const Fractions& rhs);
        friend istream &operator>>(istream &os,Fractions &s);
        bool operator>(const Fractions& rhs);
        bool operator<(const Fractions& rhs);
        bool operator==(const Fractions& rhs);
        bool operator<=(const Fractions& rhs);
        bool operator>=(const Fractions& rhs);
        void set_x(string z);
        string get_x();
        void simplify();
    protected:

    private:
        int gcd(int x,int y);
        void ne(string s);
        string x;
        int nu;
        int du;
};

#endif // FRACTIONS_H
